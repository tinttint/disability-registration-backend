/**
 * 
 *//*
package com.javahelps.restservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


*//**
 * @author jinnysaw
 * DateTime 2018Feb07 11:10PM
 *//*
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
//	@Autowired
//	private Environment env;
	
	private static final String[] PUBLIC_MATCHERS={
//			"/css/**",
//			"/js/**",
//			"/quiz/**",
//			"/user/**",
			"/**",
//			"/token/**"
			"/user/**",
			"/token/**"
	};
	
	@Override
	protected void configure(HttpSecurity http)throws Exception{
		http.csrf().disable().cors().disable().httpBasic().and().
		authorizeRequests().antMatchers(PUBLIC_MATCHERS).permitAll()
		.antMatchers("/registration").permitAll().
		antMatchers("/token").permitAll()
		.anyRequest().authenticated();
	}
	
	
	@Bean
	public HttpSessionStrategy httpSessionStrategy(){
		return new HeaderHttpSessionStrategy();
	}
}
*/