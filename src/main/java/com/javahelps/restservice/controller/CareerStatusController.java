package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.CareerStatus;
import com.javahelps.restservice.repository.CareerStatusRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/careerstatus")
public class CareerStatusController {

    @Autowired
    private CareerStatusRepository repository;

    @GetMapping
    public Iterable<CareerStatus> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<CareerStatus> find(@PathVariable("Id") Integer careerId) {
        return repository.findById(careerId);
    }

    @PostMapping(consumes = "application/json")
    public CareerStatus create(@RequestBody CareerStatus careerStatus) {
        return repository.save(careerStatus);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer careerId) {
        repository.deleteById(careerId);
    }

    @PutMapping(path = "/{Id}")
    public CareerStatus update(@PathVariable("Id") Integer careerId, @RequestBody CareerStatus careerStatus) throws BadHttpRequest {
        if (repository.existsById(careerId)) {
        	careerStatus.setId(careerId);
            return repository.save(careerStatus);
        } else {
            throw new BadHttpRequest();
        }
    }
}