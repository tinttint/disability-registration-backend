package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.Disability;
import com.javahelps.restservice.repository.DisabilityRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/disability")
public class DisabilityController{
	
	@Autowired
	private DisabilityRepository repository;
	
	@GetMapping
	public Iterable<Disability> findAll()
	{
		return repository.findAll();
	}
	
	@GetMapping(path = "/{Id}")
    public Optional<Disability> find(@PathVariable("Id") Integer disabilityId) {
        return repository.findById(disabilityId);
    }

    @PostMapping(consumes = "application/json")
    public Disability create(@RequestBody Disability disability) {
        return repository.save(disability);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer disabilityId) {
        repository.deleteById(disabilityId);
    }

    @PutMapping(path = "/{Id}")
    public Disability update(@PathVariable("Id") Integer disabilityId, @RequestBody Disability disability) throws BadHttpRequest {
        if (repository.existsById(disabilityId)) {
        	disability.setId(disabilityId);
            return repository.save(disability);
        } else {
            throw new BadHttpRequest();
        }
    }
}