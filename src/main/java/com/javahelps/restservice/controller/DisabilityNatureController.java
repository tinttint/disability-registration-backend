package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.DisabilityNature;
import com.javahelps.restservice.repository.DisabilityNatureRepository;

import javassist.tools.web.BadHttpRequest;


@RestController
@RequestMapping(path = "/disabilitynature")
public class DisabilityNatureController{
	
	@Autowired
	private DisabilityNatureRepository repository;
	
	@GetMapping
	public Iterable<DisabilityNature> findAll()
	{
		return repository.findAll();
	}
	
	@GetMapping(path = "/{Id}")
    public Optional<DisabilityNature> find(@PathVariable("Id") Integer disabilityNatureId) {
        return repository.findById(disabilityNatureId);
    }

    @PostMapping(consumes = "application/json")
    public DisabilityNature create(@RequestBody DisabilityNature disabilityNature) {
        return repository.save(disabilityNature);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer disabilityNatureId) {
        repository.deleteById(disabilityNatureId);
    }
	
    @PutMapping(path = "/{Id}")
    public DisabilityNature update(@PathVariable("Id") Integer disabilityNatureId, @RequestBody DisabilityNature disabilityNature) throws BadHttpRequest {
        if (repository.existsById(disabilityNatureId)) {
        	disabilityNature.setId(disabilityNatureId);
            return repository.save(disabilityNature);
        } else {
            throw new BadHttpRequest();
        }
    }
}