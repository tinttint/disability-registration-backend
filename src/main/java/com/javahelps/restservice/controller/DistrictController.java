package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.District;
import com.javahelps.restservice.repository.DistrictRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/district")
public class DistrictController {

    @Autowired
    private DistrictRepository repository;

    @GetMapping
    public Iterable<District> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<District> find(@PathVariable("Id") Integer districtId) {
        return repository.findById(districtId);
    }

    @PostMapping(consumes = "application/json")
    public District create(@RequestBody District district) {
        return repository.save(district);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer districtId) {
        repository.deleteById(districtId);
    }

    @PutMapping(path = "/{Id}")
    public District update(@PathVariable("Id") Integer districtId, @RequestBody District district) throws BadHttpRequest {
        if (repository.existsById(districtId)) {
        	district.setId(districtId);
            return repository.save(district);
        } else {
            throw new BadHttpRequest();
        }
    }
}