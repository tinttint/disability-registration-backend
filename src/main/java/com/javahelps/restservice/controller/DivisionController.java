package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.Division;
import com.javahelps.restservice.repository.DivisionRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/division")
public class DivisionController {

    @Autowired
    private DivisionRepository repository;

    @GetMapping
    public Iterable<Division> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<Division> find(@PathVariable("Id") Integer divisionId) {
        return repository.findById(divisionId);
    }

    @PostMapping(consumes = "application/json")
    public Division create(@RequestBody Division division) {
        return repository.save(division);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer divisionId) {
        repository.deleteById(divisionId);
    }

    @PutMapping(path = "/{Id}")
    public Division update(@PathVariable("Id") Integer divisionId, @RequestBody Division division) throws BadHttpRequest {
        if (repository.existsById(divisionId)) {
        	division.setId(divisionId);
            return repository.save(division);
        } else {
            throw new BadHttpRequest();
        }
    }
}