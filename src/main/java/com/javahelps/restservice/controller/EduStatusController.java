package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.EduStatus;
import com.javahelps.restservice.repository.EduStatusRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/edustatus")
public class EduStatusController {

    @Autowired
    private EduStatusRepository repository;

    @GetMapping
    public Iterable<EduStatus> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<EduStatus> find(@PathVariable("Id") Integer eduStatusId) {
        return repository.findById(eduStatusId);
    }

    @PostMapping(consumes = "application/json")
    public EduStatus create(@RequestBody EduStatus eduStatus) {
        return repository.save(eduStatus);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer eduStatusId) {
        repository.deleteById(eduStatusId);
    }

    @PutMapping(path = "/{Id}")
    public EduStatus update(@PathVariable("Id") Integer eduStatusId, @RequestBody EduStatus eduStatus) throws BadHttpRequest {
        if (repository.existsById(eduStatusId)) {
        	eduStatus.setId(eduStatusId);
            return repository.save(eduStatus);
        } else {
            throw new BadHttpRequest();
        }
    }
}