package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.Need;
import com.javahelps.restservice.repository.NeedRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/need")
public class NeedController {

    @Autowired
    private NeedRepository repository;

    @GetMapping
    public Iterable<Need> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<Need> find(@PathVariable("Id") Integer needId) {
        return repository.findById(needId);
    }

    @PostMapping(consumes = "application/json")
    public Need create(@RequestBody Need need) {
        return repository.save(need);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer needId) {
        repository.deleteById(needId);
    }

    @PutMapping(path = "/{Id}")
    public Need update(@PathVariable("Id") Integer needId, @RequestBody Need need) throws BadHttpRequest {
        if (repository.existsById(needId)) {
        	need.setId(needId);
            return repository.save(need);
        } else {
            throw new BadHttpRequest();
        }
    }

}