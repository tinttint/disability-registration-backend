package com.javahelps.restservice.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.Need;
import com.javahelps.restservice.entity.NeedOfPerson;
import com.javahelps.restservice.repository.NeedOfPersonRepository;
import com.javahelps.restservice.repository.NeedRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/needofperson")
public class NeedOfPersonController {

    @Autowired
    private NeedOfPersonRepository needOfPersonRepository;
    
    @Autowired
    private NeedRepository needRepository;

    @GetMapping
    public Iterable<NeedOfPerson> findAll() {
        return needOfPersonRepository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<NeedOfPerson> find(@PathVariable("Id") Integer needOfPersonId) {
        return needOfPersonRepository.findById(needOfPersonId);
    }

    @PostMapping(consumes = "application/json")
    public NeedOfPerson create(@Valid @RequestBody NeedOfPerson needOfPerson) {
    	System.out.println(" Need Of Person Object : " + needOfPerson);
    	//Need need = needRepository.findOne(needOfPerson.getNeed().getId());
    	//needOfPerson.setNeed(need);
    	return needOfPersonRepository.save(needOfPerson);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer needOfPersonId) {
    	needOfPersonRepository.deleteById(needOfPersonId);
    }

    @PutMapping(path = "/{Id}")
    public NeedOfPerson update(@PathVariable("Id") Integer needOfPersonId, @RequestBody NeedOfPerson needOfPerson) throws BadHttpRequest {
        if (needOfPersonRepository.existsById(needOfPersonId)) {
        	needOfPerson.setId(needOfPersonId);
            return needOfPersonRepository.save(needOfPerson);
        } else {
            throw new BadHttpRequest();
        }
    }

}