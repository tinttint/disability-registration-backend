package com.javahelps.restservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.Person;
import com.javahelps.restservice.repository.PersonRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/person")
public class PersonController {

    @Autowired
    private PersonRepository repository;

    @GetMapping
    public Iterable<Person> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<Person> find(@PathVariable("Id") Integer personId) {
        return repository.findById(personId);
    }
    
    @GetMapping(path = "/{Id}/{disabilityId}/{needId}/{divisionId}/{districtId}/{townshipId}/{eduStatusId}/{careerStatusId}")
    public List<Person> findIndividualDetail(@PathVariable("Id") Integer personId, @PathVariable("disabilityId") Integer disabilityId, @PathVariable("needId") Integer needId, @PathVariable("divisionId") Integer divisionId, @PathVariable("districtId") Integer districtId, @PathVariable("townshipId") Integer townshipId, @PathVariable("eduStatusId") Integer eduStatusId, @PathVariable("careerStatusId") Integer careerStatusId) {
    	return (List<Person>) repository.individualDetailReport(personId, disabilityId, needId, divisionId, districtId, townshipId, eduStatusId, careerStatusId);
    }

    @GetMapping(path = "/{natureId}/{eduStatusId}/{careerStatusId}")
    public double analysisReport(@PathVariable("natureId") Integer natureId,@PathVariable("eduStatusId") Integer eduStatusId,@PathVariable("careerStatusId") Integer careerStatusId) {
    	List<Object[]> list = null;
    	double result = 0.0;
    	double natureCount=0.0,personCount=0.0;
    	
    	if(natureId != null)
    		list = repository.reportForNature(natureId);
    	else if(eduStatusId != null)
    		list = repository.reportForEduStatus(eduStatusId);
    	else if(careerStatusId != null)
    		list = repository.reportForCareerStatus(careerStatusId);    	
    	
    	for(int i=0;i<list.size();i++)
    	{
    		Object[] obj = list.get(i);
    		for(int j=0;j<obj.length;j++)
    		{
    			String pCount = obj[0].toString();
    			personCount = Double.parseDouble(pCount);
    			String nCount = obj[1].toString();
    			natureCount = Double.parseDouble(nCount);
    		}
    	}
    	
    	result = (natureCount/personCount)*100;
    	return result;
    }
    
    @PostMapping(consumes = "application/json")
    public Person create(@RequestBody Person person) {
        return repository.save(person);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer personId) {
        repository.deleteById(personId);
    }

    @PutMapping(path = "/{Id}")
    public Person update(@PathVariable("Id") Integer personId, @RequestBody Person person) throws BadHttpRequest {
        if (repository.existsById(personId)) {
            person.setId(personId);
            return repository.save(person);
        } else {
            throw new BadHttpRequest();
        }
    }
}