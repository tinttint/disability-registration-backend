package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.Township;
import com.javahelps.restservice.repository.TownshipRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/township")
public class TownshipController {

    @Autowired
    private TownshipRepository repository;

    @GetMapping
    public Iterable<Township> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<Township> find(@PathVariable("Id") Integer townshipId) {
        return repository.findById(townshipId);
    }

    @PostMapping(consumes = "application/json")
    public Township create(@RequestBody Township township) {
        return repository.save(township);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer townshipId) {
        repository.deleteById(townshipId);
    }

    @PutMapping(path = "/{Id}")
    public Township update(@PathVariable("Id") Integer townshipId, @RequestBody Township township) throws BadHttpRequest {
        if (repository.existsById(townshipId)) {
        	township.setId(townshipId);
            return repository.save(township);
        } else {
            throw new BadHttpRequest();
        }
    }

}