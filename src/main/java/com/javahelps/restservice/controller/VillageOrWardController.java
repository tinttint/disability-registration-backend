package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.VillageOrWard;
import com.javahelps.restservice.repository.VillageOrWardRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/villageorward")
public class VillageOrWardController {

    @Autowired
    private VillageOrWardRepository repository;

    @GetMapping
    public Iterable<VillageOrWard> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<VillageOrWard> find(@PathVariable("Id") Integer villageOrWardId) {
        return repository.findById(villageOrWardId);
    }

    @PostMapping(consumes = "application/json")
    public VillageOrWard create(@RequestBody VillageOrWard villageOrWard) {
        return repository.save(villageOrWard);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer villageOrWardId) {
        repository.deleteById(villageOrWardId);
    }

    @PutMapping(path = "/{Id}")
    public VillageOrWard update(@PathVariable("Id") Integer villageOrWardId, @RequestBody VillageOrWard villageOrWard) throws BadHttpRequest {
        if (repository.existsById(villageOrWardId)) {
        	villageOrWard.setId(villageOrWardId);
            return repository.save(villageOrWard);
        } else {
            throw new BadHttpRequest();
        }
    }

}