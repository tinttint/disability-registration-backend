package com.javahelps.restservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javahelps.restservice.entity.VillageTrackOrQuarter;
import com.javahelps.restservice.repository.VillageTrackOrQuarterRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/villagetrackorquarter")
public class VillageTrackOrQuarterController {

    @Autowired
    private VillageTrackOrQuarterRepository repository;

    @GetMapping
    public Iterable<VillageTrackOrQuarter> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{Id}")
    public Optional<VillageTrackOrQuarter> find(@PathVariable("Id") Integer villageOrQuarterId) {
        return repository.findById(villageOrQuarterId);
    }

    @PostMapping(consumes = "application/json")
    public VillageTrackOrQuarter create(@RequestBody VillageTrackOrQuarter villageOrQuarter) {
        return repository.save(villageOrQuarter);
    }

    @DeleteMapping(path = "/{Id}")
    public void delete(@PathVariable("Id") Integer villageOrQuarterId) {
        repository.deleteById(villageOrQuarterId);
    }

    @PutMapping(path = "/{Id}")
    public VillageTrackOrQuarter update(@PathVariable("Id") Integer villageOrQuarterId, @RequestBody VillageTrackOrQuarter villageOrQuarter) throws BadHttpRequest {
        if (repository.existsById(villageOrQuarterId)) {
        	villageOrQuarter.setId(villageOrQuarterId);
            return repository.save(villageOrQuarter);
        } else {
            throw new BadHttpRequest();
        }
    }

}