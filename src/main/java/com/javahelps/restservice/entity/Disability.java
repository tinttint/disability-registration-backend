package com.javahelps.restservice.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Disability implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	//@OneToMany(mappedBy="disability", cascade = CascadeType.ALL)
	//@JsonIgnore
	//private List<Person> person;
	
	//@ManyToOne 
	//private DisabilityNature disabilityNature;
	
//	public DisabilityNature getDisabilityNature() {
//		return disabilityNature;
//	}
//
//	public void setDisabilityNature(DisabilityNature disabilityNature) {
//		this.disabilityNature = disabilityNature;
//	}

	@Column(nullable = false)
	private String causeName;
	
	@Column(nullable = false)
	private String severity;
	
	@Column(nullable = false)
    private String inheritedFrom;
        
    private String photos;
    
    private String details;
    
    @Column(nullable = false)
    private int disabilityNatureId;
    
    @Column(nullable = false)
    private Double startedAge;
    
    @Column(nullable = false)
    private boolean byInheritance;    

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCauseName() {
		return causeName;
	}

	public void setCauseName(String causeName) {
		this.causeName = causeName;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getInheritedFrom() {
		return inheritedFrom;
	}

	public void setInheritedFrom(String inheritedFrom) {
		this.inheritedFrom = inheritedFrom;
	}

	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getDisabilityNatureId() {
		return disabilityNatureId;
	}

	public void setDisabilityNatureId(int disabilityNatureId) {
		this.disabilityNatureId = disabilityNatureId;
	}

	public Double getStartedAge() {
		return startedAge;
	}

	public void setStartedAge(Double startedAge) {
		this.startedAge = startedAge;
	}

	public boolean isByInheritance() {
		return byInheritance;
	}

	public void setByInheritance(boolean byInheritance) {
		this.byInheritance = byInheritance;
	}    
    
}