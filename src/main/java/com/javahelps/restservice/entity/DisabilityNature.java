package com.javahelps.restservice.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class DisabilityNature implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(nullable = false)
	private String name;
	
	private String madicalExplanation;
	
	//@OneToMany(mappedBy="disabilityNature", cascade = CascadeType.ALL)
	//@JsonIgnore
	//private List<Disability> disability;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMadicalExplanation() {
		return madicalExplanation;
	}
	public void setMadicalExplanation(String madicalExplanation) {
		this.madicalExplanation = madicalExplanation;
	}
	
}