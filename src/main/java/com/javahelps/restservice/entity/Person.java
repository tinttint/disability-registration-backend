package com.javahelps.restservice.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Person implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
    
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String oldNrc;
	
	@Column(nullable = false)
    private String nrc;
    
    @Column(nullable = false)
    private String fatherName;
    
    @Column(nullable = false)
    private String maritalStatus;
    
    @Column(nullable = false)
    private String guardianName;
    
    @Column(nullable = false)
    private String guardianRelation;
    
    @Column(nullable = false)
    private boolean gender;
    
    @Column(nullable = false)
    private Date dateOfBirth;
        
    @Column(nullable = false)
    private int divisionId;
    
    @Column(nullable = false)
    private int districtId;
    
    @Column(nullable = false)
    private int townshipId;    
    
    @Column(nullable = false)
    private int disabilityId;
    
    @Column(nullable = false)
    private int eduStatusId;
    
    @Column(nullable = false)
    private int careerStatusId;
    
    private byte[] photos; //(base-64)
    
    public byte[] getPhotos() {
		return photos;
	}
	public void setPhotos(byte[] photos) {
		this.photos = photos;
	}

	private int villageTrackOrQuarterId;
    private int villageOrWardId;
    private String motherName;
    private String otherId;
    
    @OneToMany(mappedBy="person", cascade = CascadeType.ALL)
	private List<NeedOfPerson> needOfPersons;
    
    public List<NeedOfPerson> getNeedOfPersons() {
		return needOfPersons;
	}
	public void setNeedOfPersons(List<NeedOfPerson> needOfPersons) {
		this.needOfPersons = needOfPersons;
	}
	
    //@ManyToOne 
	//private Division division;   
	
	//@ManyToOne 
	//private District district;
    
    //@ManyToOne 
	//private Township township;
    
    //@ManyToOne 
	//private EduStatus eduStatus;
    
    //@ManyToOne 
	//private CareerStatus careerStatus;
    
    //@ManyToOne 
	//private VillageOrWard villageOrWard;
    
    //@ManyToOne 
	//private VillageTrackOrQuarter villageOrQuarter;
    
    //@ManyToOne 
	//private Disability disability;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOldNrc() {
		return oldNrc;
	}
	public void setOldNrc(String oldNrc) {
		this.oldNrc = oldNrc;
	}
	public String getNrc() {
		return nrc;
	}
	public void setNrc(String nrc) {
		this.nrc = nrc;
	}
	public String getOtherId() {
		return otherId;
	}
	public void setOtherId(String otherId) {
		this.otherId = otherId;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	
	public String getGuardianName() {
		return guardianName;
	}
	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}
	public String getGuardianRelation() {
		return guardianRelation;
	}
	public void setGuardianRelation(String guardianRelation) {
		this.guardianRelation = guardianRelation;
	}
	public boolean isGender() {
		return gender;
	}
	public void setGender(boolean gender) {
		this.gender = gender;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public int getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(int divisionId) {
		this.divisionId = divisionId;
	}
	public int getDistrictId() {
		return districtId;
	}
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	public int getTownshipId() {
		return townshipId;
	}
	public void setTownshipId(int townshipId) {
		this.townshipId = townshipId;
	}
	public int getVillageTrackOrQuarterId() {
		return villageTrackOrQuarterId;
	}
	public void setVillageTrackOrQuarterId(int villageTrackOrQuarterId) {
		this.villageTrackOrQuarterId = villageTrackOrQuarterId;
	}
	public int getVillageOrWardId() {
		return villageOrWardId;
	}
	public void setVillageOrWardId(int villageOrWardId) {
		this.villageOrWardId = villageOrWardId;
	}
	public int getDisabilityId() {
		return disabilityId;
	}
	public void setDisabilityId(int disabilityId) {
		this.disabilityId = disabilityId;
	}
	public int getEduStatusId() {
		return eduStatusId;
	}
	public void setEduStatusId(int eduStatusId) {
		this.eduStatusId = eduStatusId;
	}
	public int getCareerStatusId() {
		return careerStatusId;
	}
	public void setCareerStatusId(int careerStatusId) {
		this.careerStatusId = careerStatusId;
	}
	
    @Override
    public String toString() {
    	return "Name : "+ name + " Nrc: " + nrc + " Father Name : " + fatherName + " Gender : " + gender + " Date Of Birth : " + dateOfBirth;
    }
	
}