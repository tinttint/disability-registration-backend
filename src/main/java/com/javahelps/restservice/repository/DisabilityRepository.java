package com.javahelps.restservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.javahelps.restservice.entity.Disability;

@RepositoryRestResource(exported = false)
public interface DisabilityRepository extends JpaRepository<Disability, Integer>{

	//List<Course> findCourseBySchoolId(Long schoolid);

	//void delete(String name);

	//boolean exists(String name);

}