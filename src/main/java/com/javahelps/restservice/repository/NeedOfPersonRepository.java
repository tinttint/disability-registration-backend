package com.javahelps.restservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.javahelps.restservice.entity.NeedOfPerson;

@RepositoryRestResource(exported = false)
public interface NeedOfPersonRepository extends JpaRepository<NeedOfPerson, Integer>{


}