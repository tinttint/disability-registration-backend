package com.javahelps.restservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.javahelps.restservice.entity.Person;

@RepositoryRestResource(exported = false)
public interface PersonRepository extends JpaRepository<Person, Integer>{
	@Query(nativeQuery=true, value="SELECT * FROM person"
			+ " inner join township on person.township_id=township.id"
			+ " inner join division on person.division_id=division.id"
			+ " inner join district on person.district_id=district.id"
			+ " inner join need_of_person on person.id = need_of_person.person_id"
			+ " inner join need on need.id = need_of_person.need_id"
			+ " inner join edu_status on edu_status.id = person.edu_status_id"
			+ " inner join career_status on career_status.id = person.career_status_id"
			+ " inner join disability on person.disability_id = disability.id"
			+ " inner join disability_nature on disability_nature.id = disability.disability_nature_id"
			+ " where (:personId is null or person.id =:personId) and (:disabilityId is null or"
			+ " disability.disability_nature_id=:disabilityId) and (:needId is null or need_of_person.need_id=:needId)"
			+ " and (:divisionId is null or person.division_id=:divisionId) and (:districtId is null or"
			+ " person.district_id=:districtId) and (:townshipId is null or person.township_id=:townshipId) and"
			+ " (:eduStatusId is null or person.edu_status_id=:eduStatusId) and (:careerStatusId is null or person.career_status_id=:careerStatusId)")
	List<Person> individualDetailReport(@Param("personId") Integer personId, @Param("disabilityId") Integer disabilityId, @Param("needId") Integer needId, @Param("divisionId") Integer divisionId, @Param("districtId") Integer districtId, @Param("townshipId") Integer townshipId, @Param("eduStatusId") Integer eduStatusId, @Param("careerStatusId") Integer careerStatusId);
	
	@Query(nativeQuery=true, value="select SUM(person.id != 0) AS totalCount,"
			+" SUM(disability_nature.id = :natureId) AS disNatureCount"
			+" FROM person"
			+" inner join disability on person.disability_id=disability.id" 
			+" inner join disability_nature on disability.disability_nature_id=disability_nature.id"
			+" WHERE person.id != 0 OR disability_nature.id = :natureId")
	List<Object[]> reportForNature(@Param("natureId") Integer natureId);
	
	@Query(nativeQuery=true, value="select SUM(person.id != 0) AS totalCount,"
			+" SUM(person.edu_status_id = :eduStatusId) AS eduStatusCount"
			+" FROM person" 
			+" WHERE person.id != 0 OR person.edu_status_id = :eduStatusId")
	List<Object[]> reportForEduStatus(@Param("eduStatusId") Integer eduStatusId);
	
	@Query(nativeQuery=true, value="select SUM(person.id != 0) AS totalCount,"
			+" SUM(person.career_status_id = :careerStatusId) AS careerStatusCount"
			+" FROM person"
			+" WHERE person.id != 0 OR person.career_status_id = :careerStatusId")
	List<Object[]> reportForCareerStatus(@Param("careerStatusId") Integer careerStatusId);
	
}
