package com.javahelps.restservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.javahelps.restservice.entity.Township;

@RepositoryRestResource(exported = false)
public interface TownshipRepository extends JpaRepository<Township, Integer>{


}